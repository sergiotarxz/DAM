"""
Resolución a mas alto nivel que la permitida en nuestro pseudocódigo
Creditos a:
 ___________________________________________________
|El Club del Autodidacta
|http://elclubdelautodidacta.es/wp/
|
|BitBite Python #2:
|
|sumacifras.py
|Javier Montero
|___________________________________________________
Comentarios en http://elclubdelautodidacta.es/wp/2012/03/bitbite-python-2-sumando-las-cifras/ ‎

Adaptado por Sergio Iglesias
"""


numero=input('Introduce un número de las cifras que quieras: ')

suma=0

for cifra in numero:
    if int(cifra) % 2 == 0:
        suma+=int(cifra)
print(suma)
