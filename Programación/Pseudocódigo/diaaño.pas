program diano;
var
   mes	     : integer;
   dia	     : integer;
   resultado : integer;
begin
   writeln('Introduzca mes en formato numérico');
   readln(mes);
   writeln('Introduzca dia');
   readln(dia);
   case mes of
     1	: resultado := dia;
     2	: resultado := 31 + dia;
     3	: resultado := 31 + 28 + dia;
     4	: resultado := 31 + 28 + 31 + dia;
     5	: resultado := 31 + 28 + 31 + 30 + dia;
     6	: resultado := 31 + 28 + 31 + 30 + 31 + dia;
     7	: resultado := 31 + 28 + 31 + 30 + 31 + 30 + dia;
     8	: resultado := 31 + 28 + 31 + 30 + 31 + 30 + 31 + dia;
     9	: resultado := 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + dia;
     10	: resultado := 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + dia;
     11	: resultado := 31 + 28 + 31 + 30 + 31 + 30 + 31	+ 31 + 30 + 31 + dia;
     12	: resultado := 31 + 28 + 31 + 30 + 31 + 30 + 31	+ 31 + 30 + 31 + 30 + dia;
   end;
   writeln('Hoy es' , resultado);
end.
