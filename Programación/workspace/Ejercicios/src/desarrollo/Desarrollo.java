package desarrollo;



import principal.Ejercicios;

public class Desarrollo {

	public static void ejercicio1()
	{
		int x;
		int y;
		System.out.println("Introduzca dos números enteros para saber cual es mayor");
		x = Ejercicios.entrada.nextInt();
		y = Ejercicios.entrada.nextInt();
	
		if (x == y)
		{
			System.out.println("Son iguales.");
		}
		else if (x > y)
		{
			System.out.println(x + " mayor que " + y);
		}
		else
		{
			System.out.println(y + " mayor que " + x);
		}
	}
	public static void ejercicio2()
	{
		double x;
		System.out.println("Escriba su nota por favor.");
		x = Ejercicios.entrada.nextDouble();
		if (x < 3)
		{
			System.out.println("Muy (in)suficiente");
		}
		else if (x >= 3 && x < 5)
		{
			System.out.println("Suspenso");
		}
		else if (x >= 5 && x < 6)
		{
			System.out.println("Suficiente");
		}
		else if (x >= 6 && x <7)
		{
			System.out.println("Bien");
		}
		else if (x >= 7 && x < 9)
		{
			System.out.println("Notable");
		}
		else if (x >= 9) 
		{
			System.out.println("Sobresaliente");
		}
	}
	public static void ejercicio3()
	{
		System.out.println("Introduzca el año para saber si es bisiesto");
		int ano;
		ano = Ejercicios.entrada.nextInt();
		if (ano % 4 == 0 && (!(ano % 100 == 0) || ano % 400 == 0))
		{
			System.out.println("Es bisiesto");
		}
		else
		{
			System.out.println("No es bisiesto"); 
		}
		
	} 
	public static void ejercicio4()
	{
		int i = 1;
		while (i < 20)
		{
			if (i % 2 == 0)
			{
				System.out.println("" + i);
			}
			i++;
		}
	}
	public static void ejercicio5()
	{
		int n, sumap, sumai, i;
		System.out.println("Introduzca un límite positivo distinto de cero");
		n = Ejercicios.entrada.nextInt();
		i = 1;
		sumap = 0;
		sumai = 0;
		while (i < n)
		{
			if (i % 2 == 0)
			{
				sumap =+ i;
			}
			else
			{
				sumai =+ i;
			}
			i++;
		}
		System.out.println("La suma de los numeros pares menores que " + n + " es " + sumap);
		System.out.println("La suma de los numeros impares menores que " + n + " es " + sumai);
	}
	public static void ejercicio6()
	{
		int n, numerop, numeroi;
		numerop = 0;
		numeroi = 0;
		n = 1;
		System.out.println("Esta apunto de entrar en un bucle que le pedirá numeros enteros positivos hasta que introduzca 0\n"
				+ "(0 es el único numero exclusivo del conjunto de los numeros enteros que afecta al funcionamiento del programa");
		while (n != 0)
		{
			System.out.println("Introduzca un número");
			n = Ejercicios.entrada.nextInt();
			if (n > 0 )
			{
				if (n % 2 == 0 && n != 0)
				{
					numerop++;
				}
				else if (n != 0)
				{
					numeroi++;
				}
			}
		}
		System.out.println("Número pares " + numerop + " Número impares " + numeroi);
	}
	public static void ejercicio7()
	{
		int total, entrada;
		entrada = 0;
		total = 0;
		while (entrada >= 0)
		{
			System.out.println("Introduzca un número, negativo para escapar del bucle.");
			entrada = Ejercicios.entrada.nextInt();
			if (entrada % 5 == 0)
			{
				total += entrada;
			}
			else if (entrada > 0)
			{
				System.out.println(entrada + " No es múltiplo de 5");
			}
					
		}
		System.out.println("La suma de los multiplos de 5 es" + total);
				
	}
	public static void ejercicio8()
	{
		int entrada, mayor;
		String texto;
		texto = "";
		mayor = 0;
		while (texto.toUpperCase().compareTo("Y") != 0)
		{
			System.out.println("Introduzca un número");
			entrada = Ejercicios.entrada.nextInt();
			if (entrada % 2 == 0 && entrada > mayor)
			{
				mayor = entrada;
				System.out.println("Actualizado el mayor de los pares");
			}
			System.out.println("¿Desea salir?(y/Y para salir)");
			texto = Ejercicios.entrada.next();
		}
		if (mayor == 0)
		{
			System.out.println("No has introducido numeros pares mayores que 0");
		}
		else
		{
			System.out.println("El mayor es " + mayor);
		}
	}
	public static void ejercicio9()
	{
		int s, m, h;
		m = 0;
		h = 0;
		System.out.println("Introduzca un numero de segundos para transformarlos a horas'minutos'segundos");
		s = Ejercicios.entrada.nextInt();
		if (s >= 3600) 
		{
			h = s / 3600;
			s = s % 3600;
		}
		if (s >= 60)
		{
			m = s / 60;
			s = s % 60;
		}
		System.out.println(h + "'" + m + "'" + s);
	}
	public static void ejercicio10()
	{
		int sumap, sumai, entradamayor, entradamenor, bucle;
		sumap = 0;
		sumai = 0;
		System.out.println("Este programa muestra la suma de los pares y la suma de los impares contenidos en un rango de numeros");
		System.out.println("Introduzca el primer delimitador del rango");
		entradamayor = Ejercicios.entrada.nextInt();
		System.out.println("Introduzca el segundo delimitador del rango");
		entradamenor = Ejercicios.entrada.nextInt();
		int comparacion = entradamayor - entradamenor;
		if (comparacion == 1 || comparacion == -1 || comparacion == 0 )
		{
			System.out.println("La diferencia entre los números es demasiado pequeña");
		}
		else
		{
			if (entradamayor < entradamenor)
			{
				entradamayor = entradamayor + entradamenor;
				entradamenor = entradamayor - entradamenor;
				entradamayor = entradamayor - entradamenor;
			}
			bucle = entradamenor + 1;
			while (bucle < entradamayor)
			{
				if (bucle % 2 == 0)
				{
					sumap += bucle;
				}
				else
				{
					sumai += bucle;
				}
				bucle++;
			}
			System.out.println("La suma de los pares es " + sumap + ", la de los impares " + sumai);
		}
	}
	public static void ejercicio11()
	{
		int menortres, mayorcinco, entrada;
		String salida;
		menortres = 1;
		mayorcinco = 1;
		salida = "N";
		while (salida.toLowerCase().compareTo("y") != 0)
		{
			System.out.println("Introduzca un numero");
			entrada = Ejercicios.entrada.nextInt();
			if (entrada % 3 == 0 && (entrada < menortres || (menortres == 1 && entrada > 1)))
			{
				menortres = entrada;
				System.out.println("Nuevo multiplo de tres mas pequeño");
			}
			if (entrada % 5 == 0 && (entrada > mayorcinco || (mayorcinco == 1 && entrada > 1)))
			{
				mayorcinco = entrada;
				System.out.println("Nuevo multiplo de cinco mas grande");
			}
			System.out.println("¿Quiere salir? (y/Y)");
			salida = Ejercicios.entrada.next();
		}
		System.out.println("Mayor multiplos de cinco " + mayorcinco + ", menor multiplos de tres " + menortres);
		
	}
	public static void ejercicio12()
	{
		int mayor, entrada, contador;
		String salir;
		entrada = 0;
		contador = 0;
		mayor = 0;
		salir = "n";
		while (salir.toLowerCase().compareTo("y")!=0)
		{
			System.out.println("Por favor introduzca un número");
			entrada = Ejercicios.entrada.nextInt();
			if (entrada == mayor)
			{
				contador++;
			}
			else if (entrada > mayor)
			{
				mayor = entrada;
				contador = 1;
			}
			System.out.println("¿Desea salir?(Y/y)");
			salir = Ejercicios.entrada.next();
		}
		System.out.println("El mayor número es " + mayor + ", se repite " + contador + " veces");
	}
	public static void ejercicio13()
	{
		int contador = 1;
		while (contador <= 100)
		{
			if (contador % 5 == 0 && contador % 3 != 0)
			{
				System.out.println(contador);
			}
			contador++;
		}
	}

	public static void ejercicio15()
	{
		System.out.println("Introduzca un número");
		char[] entrada = Ejercicios.entrada.next().toCharArray();
		int contador = 0;
		boolean capicua = true;
		while (((entrada.length + 1) % 2 == 0 && contador <= (entrada.length + 1) / 2) || (entrada.length + 1 % 2 != 0 && contador != (entrada.length + 1) / 2 + 1 ))
		{
			if (entrada[contador] != entrada[entrada.length -1 - contador])
			{
				capicua = false;
				  break;
			}
				contador++;
		}	
		if (capicua)
		{
			System.out.println("Es capicua");
		}
		else 
		{
			System.out.println("No es capicua");
		}
	}

}
