package principal;
import java.util.Scanner;

public class Ejercicios {
public static Scanner entrada = new Scanner(System.in);
	public static void main(String[] args) {
		int ejercicio = 0;
		int contador = 0;
		System.out.println("Escriba el numero de ejercicio que desee comprobar");
		while (ejercicio > 15 || ejercicio <= 0 )
		{
			if (contador != 0) 
					{
				System.out.println("Número incorrecto");
					}
			ejercicio = entrada.nextInt();
		}
		Ejercicios.LanzarEjercicio(ejercicio);
	}
	
	public static void LanzarEjercicio(int ejercicio)
	{
		switch (ejercicio){
		case 1: desarrollo.Desarrollo.ejercicio1();
		  break;
		case 2: desarrollo.Desarrollo.ejercicio2();
		  break;
		case 3: desarrollo.Desarrollo.ejercicio3();
		  break;
		case 4: desarrollo.Desarrollo.ejercicio4();
		  break;
		case 5: desarrollo.Desarrollo.ejercicio5();
		  break;
		case 6: desarrollo.Desarrollo.ejercicio6();
		  break;
		case 7: desarrollo.Desarrollo.ejercicio7();
		  break;
		case 8: desarrollo.Desarrollo.ejercicio8();
		  break;
		case 9: desarrollo.Desarrollo.ejercicio9();
		  break;
		case 10: desarrollo.Desarrollo.ejercicio10();
		  break;
		case 11: desarrollo.Desarrollo.ejercicio11();
		  break;
		case 12: desarrollo.Desarrollo.ejercicio12();
		  break;
		case 13: desarrollo.Desarrollo.ejercicio13();
		  break;
		case 15: desarrollo.Desarrollo.ejercicio15();
		  break;
		}
	}
}
