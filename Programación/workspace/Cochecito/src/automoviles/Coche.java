package automoviles;

public class Coche {
	private String mar;
	private String mod;
	public void coche(String marca, String modelo)
	{
		mar=marca;
		mod=modelo;
	}
	public String getMarca()
	{
		return mar;
	}
	public String getModelo()
	{
		return mod;
	}
	public void setmarca(String marca)
	{
		mar=marca;
	}
	public void setmodelo(String modelo)
	{
		mod=modelo;
	}
	public static void main(String[] args)
	{
		Coche c1 = new Coche();
		c1.setmarca("Ford");
		c1.setmodelo("Mondeo");
		Coche c2 = new Coche();
		c2.setmarca("Renault");
		c2.setmodelo("Clio");
		System.out.println("El coche 1 es un " + c1.getMarca() + " " + c1.getModelo());
		System.out.println("El coche 2 es un " + c2.getMarca() + " " + c2.getModelo());

	}
}
