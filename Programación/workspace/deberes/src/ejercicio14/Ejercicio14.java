package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 
{
	static public void main(String[] args)
	{
		int sumadivisores1;
		int sumadivisores2;
		int num1;
		int num2;
		int divisor1;
		int divisor2;
		Scanner entrada = new Scanner(System.in);
		String salida = "";
		while (salida.toLowerCase().compareTo("y") != 0)
		{
		System.out.println("Introduzca el primer número entero positivo.");
		num1 = entrada.nextInt();
		while (num1 <= 0)
		{
			System.out.println("Formato incorrecto, vuelvalo a intentar.");
			num1 = entrada.nextInt();
		}
		System.out.println("Introduzca el segundo número entero positivo.");
		num2 = entrada.nextInt();
		while (num2 <= 0)
		{
			System.out.println("Formato incorrecto, vuelvalo a intentar.");
			num2 = entrada.nextInt();
		}
		sumadivisores1 = 0;
		divisor1 = 1;
		while (divisor1 < num1)
		{
			if (num1 % divisor1 == 0)
			{
				sumadivisores1+=divisor1;
			}
			divisor1++;
		}
		divisor2 = 1;
		sumadivisores2 = 0;
		while (divisor2 < num2)
		{
			if (num2 % divisor2 == 0)
			{
				sumadivisores2+=divisor2;
			}
			divisor2++;
		}
		if (sumadivisores1 == num2 && sumadivisores2 == num1)
		{
			System.out.println("Los dos números son amigos ¡Qué bonita es la amistad!");
		}
		else
		{
			System.out.println("Sus dos números no tienen una relación de amistad");
		}
		System.out.println("¿Desea terminar ya?(Y/y)");
		salida = entrada.next();
		entrada.close();
		}
	}
}
