package dado;

import java.util.Random;
import java.util.Scanner;

public class Dado 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Random aleatorio = new Random();
		int dado;
		int n1=0;
		int n2=0;
		int n3=0;
		int n4=0;
		int n5=0;
		int n6=0;
		System.out.println("¿Cuantos dados desea tirar?");
		Scanner entrada = new Scanner(System.in);
		int num = entrada.nextInt();
		while (num < 1)
		{
			System.out.println("Número demasiado pequeño, por favor vuelvalo a intentar.");
			num = entrada.nextInt();
		}
		for (int contador = 1; contador <= num ; contador++)
		{
			dado = (int)(aleatorio.nextDouble() * 6 + 1);
			switch (dado)
			{
			case 1: n1++;
				break;
			case 2: n2++;
				break;
			case 3: n3++;
				break;
			case 4: n4++;
				break;
			case 5: n5++;
				break;
			case 6: n6++;
				break;
			}
		}
		System.out.printf("Ha salido uno " + n1 + " veces, un porcentaje del %.2f%%%n", ((double)n1/num)*100);
		System.out.printf("Ha salido dos " + n2 + " veces, un porcentaje del %.2f%%%n", ((double)n2/num)*100);
		System.out.printf("Ha salido tres " + n3 + " veces, un porcentaje del %.2f%%%n", ((double)n3/num)*100);
		System.out.printf("Ha salido cuatro " + n4 + " veces, un porcentaje del %.2f%%%n", ((double)n4/num)*100);
		System.out.printf("Ha salido cinco " + n5 + " veces, un porcentaje del %.2f%%%n", ((double)n5/num)*100);
		System.out.printf("Ha salido seis " + n6 + " veces, un porcentaje del %.2f%%%n", ((double)n6/num)*100);
		entrada.close();
	}

}
