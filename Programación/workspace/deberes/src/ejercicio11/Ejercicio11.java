package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {
 public static void main(String[] args)
 {
	 int contador = 0;
	 double nota;
	 int suspensos = 0;
	 int condicionales = 0;
	 Scanner entrada = new Scanner(System.in);
	 while (contador < 8)
	 {
		 System.out.println("Introduzca la nota, " + (contador + 1) + " de 8");
		 nota = entrada.nextInt();
		 while (nota > 10 || nota < 0)
		 {
			 System.out.println("Formato de nota incorrecto, haga el favor de volver a intentarlo");
			 nota = entrada.nextDouble();					 
		 }
		 if (nota < 5 && nota >= 4)
		 {
			 condicionales++;
		 }
		 if (nota < 4)
		 {
			 suspensos++;
		 }
		 contador++;
	 }
	 System.out.println("Porcentaje de suspensos, " + ((double)suspensos/8*100) + "%, porcentaje de aprobados condicionales, " + ((double)condicionales/8*100) + "%.");
	 entrada.close();
 }
}
