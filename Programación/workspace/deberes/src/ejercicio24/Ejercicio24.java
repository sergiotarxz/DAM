package ejercicio24;

import java.util.Scanner;

public class Ejercicio24 {
static public void main(String[] args)
{
	System.out.println("Introduzca un limite superior para el conjunto");
	Scanner entrada = new Scanner(System.in);
	int sum1;
	int sum2;
	int centro = 0;
	int limite = entrada.nextInt();
	for (int i=1; i < limite; i++)
	{
		sum1 = 0;
		sum2 = 0;
		for (int e = 1; e < i; e++)
		{
			sum1+=e;
		}
		for (int e = limite; e > i; e--)
		{
			sum2+=e;
		}
		if (sum1 == sum2)
		{
			centro = i;
			break;
		}
	}
	System.out.println("El centro es " + centro);
}
}
