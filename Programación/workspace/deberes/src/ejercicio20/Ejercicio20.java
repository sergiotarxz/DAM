package ejercicio20;

import java.util.Scanner;

public class Ejercicio20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String salida = "n";
		Scanner entrada = new Scanner(System.in);
		int a = 0;
		int b = 0;
		while (salida.toLowerCase().compareTo("y") != 0)
		{
			int e;
			System.out.println("Introduzca un número");
			e = entrada.nextInt();
			if (e > a)
			{
				if (a > b)
				{
					b = a;
				}
				a = e;
			}
			else if (e > b)
			{
				b = e;
			}
			System.out.println("¿Desea salir?(Y/y)");
			salida = entrada.next();
		}
		System.out.println("El mayor es " + a);
		System.out.println("El segundo mayor es " + b);
		entrada.close();
	}

}
