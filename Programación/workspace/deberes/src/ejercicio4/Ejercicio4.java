package ejercicio4;

public class Ejercicio4 {

	public static void main(String[] args) 
	{
		int x = 1;			
		int y = 7;
		int z = 10;
		double pi = 3.14159265358979;
		System.out.println("a) 2 * x + 0.5 + y -1 /5 * z");
		System.out.println("En primer lugar tendrán lugar la multiplicación 2 * X = 0 y la division 1/5 = 0 por no llevar (double) delante");
		System.out.println("Después la multiplicación (1/5) * Z = 0 (0 multiplicado por cualquier número es 0)");
		System.out.println("Quedaría 2 + 0,5 + Y - 0 = 2,5 + 7 = 9,5");
		System.out.println("Comprobacion = " + (2 * x + 0.5 + y -1 /5 * z));
		System.out.println("b) 4/Y + PI * X / Z");
		System.out.println("Primero 4/Y = 0 y PI * X = PI");
		System.out.println("Despues PI/Z = PI/10");
		System.out.println("Quedaría PI/10 + 0 = 0.314159265358979");
		System.out.println("Comprobacion = " + (4/y + pi * x / z));
		System.out.println("c) Y-2 / Z + 4 * Y / 2");
		System.out.println("Primero 2/Z = 1/5 = 0 y Y * 4 = 28");
		System.out.println("Despues 28 / 2 = 14");
		System.out.println("Finalmente 7 - 0 + 14 = 21");
		System.out.println("Comprobacion = " + (y-2/z+4*y/2));
	}

}
