package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {
	static public void main(String[] args)
	{
		int a;
		int b;
		int r = 1;
		Scanner entrada= new Scanner(System.in);
		a = entrada.nextInt();
		b = entrada.nextInt();
		if (a<b)
		{
			a = a + b;
			b = a - b;
			a = a - b;
		}
		while (r!=0)
		{
			r = a % b;
			if (r!=0) 
			{
				a = b;
				b = r;
			}
		}
		System.out.println(b);
		entrada.close();
	}
}
