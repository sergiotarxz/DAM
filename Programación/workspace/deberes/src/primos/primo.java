package primos;

import java.util.Scanner;

public class primo
{

	public static void main(String[] args)
	{
		int nprimos = 0;
		System.out.println("Introduzca un número menor o igual que cien");
		Scanner entrada = new Scanner(System.in);
		int numeromax = entrada.nextInt();
		while (numeromax > 100 || numeromax < 2)
		{
			System.out.println("Número incorrecto, por favor vuelva a intentarlo");
			numeromax = entrada.nextInt();
		}
		entrada.close();
		int contador = 2;
		while (contador  <= numeromax)
		{
			int contador2 = 2;
			boolean flag = true;
			while (contador2 < contador)
			{
				if (contador % contador2 == 0)
				{
					flag = false;
					break;
				}
				contador2++;
			}
			if (flag)
			{
				System.out.println(contador);
				nprimos++;
				
			}
			contador++;
		}
		System.out.println("Número de primos " + nprimos + " hasta " + numeromax);
	}

}
