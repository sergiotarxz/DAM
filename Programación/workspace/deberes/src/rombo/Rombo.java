package rombo;

import java.util.Scanner;

public class Rombo {

	public static void main(String[] args) {
		int contador1 = 0;
		int contador2;
		int tamano;
		Scanner entrada = new Scanner(System.in);
		System.out.println("¿Qué tamaño de rombo desea?");
		tamano = entrada.nextInt();
		entrada.close();
		if (tamano % 2 == 0) //Tamaños pares
		{
			while (contador1 <= tamano/2) //fila
			{
				contador2 = 0; //inicializo contador2 para cada iteración de fila
				int espacios = (tamano - ( 2 * contador1))/2; //Los espacios a imprimir son la mitad de los que forman el rombo que equivalen a tamaño - asteriscos
				int asteriscos =  2 * contador1; //el numero de asteriscos es el doble del numero de fila
				while (contador2 < espacios) //columna espacios
				{
					System.out.printf(" ");	
					contador2++;
				}
				contador2 = 0;
				while (contador2 < asteriscos) //columna asteriscos
				{
					System.out.printf("*");
					contador2++;
				}
				System.out.println();
				contador1++;
			}
			contador1 = tamano/2; //contador1 (filas) es la mitad del tamaño apartir de que se excede la mitad del rombo
			while (contador1 <= tamano/2 && contador1 >= 0) 
			{
				int espacios = (tamano - ( 2 * contador1))/2;
				int asteriscos =  2 * contador1;
				contador2 = 0;
				while (contador2 < espacios)
				{
					System.out.printf(" ");	
					contador2++;
				}
				contador2 = 0;
				while (contador2 < asteriscos)
				{
					System.out.printf("*");
					contador2++;
				}
				System.out.println();
				contador1--; //bajamos en uno el valor de contador1 

			}
		}
		else //tamaños impares
		{
			while (contador1 <= tamano/2) //fila
			{
				contador2 = 0;
				int espacios = (tamano - (1 + 2 * contador1))/2;
				int asteriscos = 1 + 2 * contador1;
				while (contador2 < espacios) //columna espacios
				{
					System.out.printf(" ");	
					contador2++;
				}
				contador2 = 0;
				while (contador2 < asteriscos) //columna asteriscos
				{
					System.out.printf("*");
					contador2++;
				}
				System.out.println();
				contador1++;
			}
			contador1 = tamano/2-1 ;
			while (contador1 < tamano/2 && contador1 >= 0) //fila
			{
				int espacios = (tamano - (1 + 2 * contador1))/2;
				int asteriscos = 1 + 2 * contador1;
				contador2 = 0;
				while (contador2 < espacios) //columna espacios
				{
					System.out.printf(" ");	
					contador2++;
				}
				contador2 = 0;
				while (contador2 < asteriscos) //columna asteriscos
				{
					System.out.printf("*");
					contador2++;
				}
				System.out.println();
				contador1--;

			}
		}
	}

}