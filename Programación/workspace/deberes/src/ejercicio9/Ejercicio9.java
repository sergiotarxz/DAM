package ejercicio9;

import java.util.Scanner;

public class Ejercicio9 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		int nota = 0;
		int contador = 0;
		int suspensos = 0;
		Scanner entrada = new Scanner(System.in);
		while (contador < 5)
		{
			System.out.println("Introduzca una nota, " + (contador + 1) + " de 5");
			nota = entrada.nextInt();
			while (!(nota >= 0 && nota <= 10))
			{
				System.out.println("Formato de nota incorrecto, vuelva a intentarlo");
				nota = entrada.nextInt();
			}
			if (nota < 5)
			{
				suspensos++;
			}
			contador++;

		}
		System.out.println("El número de suspensos es " + suspensos);
		
	}

}
