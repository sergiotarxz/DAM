package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {
	static public void main(String[] args)
	{
		System.out.println("Introduzca el mes de su nacimiento.");
		Scanner entrada = new Scanner(System.in);
		int mes = entrada.nextInt();
		System.out.println("Introduzca el día de su nacimiento.");
		int dia = entrada.nextInt();
		while ((mes == 2 && dia > 29) || (mes == 1 && dia > 31) || (mes > 2 && mes < 8 && mes % 2 == 0 && dia > 30) || mes < 1 ||(mes > 2 && mes < 8 && mes % 2 != 0 && dia > 31) || mes > 12 || dia < 1 || (mes > 7 && mes < 13 && mes % 2 == 0 && dia > 31)|| (mes > 7 && mes < 13 && mes % 2 != 0 && dia > 30)) //Comprueba formato dia/mes
		{
			System.out.println("Error de formato, vuelva a introducir el día de su nacimiento");
			dia = entrada.nextInt();
			System.out.println("Introduzca el mes de su nacimiento");
			mes = entrada.nextInt();
		}
		switch (mes)
		{
		case 1: 
			if (dia < 9) 
		    {
				System.out.println("Sagitario");
			}
			else
			{
				System.out.println("Capricornio");
			}
		break;
		case 2: 
			if (dia < 16)
			{
				System.out.println("Capricornio");
			}
			else
			{
				System.out.println("Acuario");
			}
		break;
		case 3:
			if (dia < 12)
			{
				System.out.println("Acuario");
			}
			else
			{
				System.out.println("Piscis");
			}
		break;
		case 4:
			if (dia < 19)
			{
				System.out.println("Piscis");
			}
			else
			{
				System.out.println("Aries");
			}
		break;
		case 5:
			if (dia < 14)
			{
				System.out.println("Aries");
			}
			else
			{
				System.out.println("Tauro");
			}
		break;
		case 6:
			if (dia < 20)
			{
				System.out.println("Tauro");
			}
			else
			{
				System.out.println("Géminis");
			}
		break;
		case 7:
			if (dia < 21)
			{
				System.out.println("Géminis");
			}
			else
			{
				System.out.println("Cancer");
			}
		break;
		case 8:
			if (dia < 10)
			{
				System.out.println("Cancer");
			}
			else
			{
				System.out.println("Leo");
			}
		break;
		case 9:
			if (dia < 15)
			{
				System.out.println("Leo");
			}
			else 
			{
				System.out.println("Virgo");
			}
		break;
		case 10:
			if (dia < 31)
			{
				System.out.println("Virgo");
			}
			else
			{
				System.out.println("Libra");
			}
		break;
		case 11:
			if (dia < 23)
			{
				System.out.println("Libra");
			}
			else if (dia < 30)
			{
				System.out.println("Escorpio");
			}
			else
			{
				System.out.println("Ofiuco");
			}
		break;
		case 12:
			if (dia < 18)
			{
				System.out.println("Ofiuco");
			}
			else
			{
				System.out.println("Sagitario");
			}
		break;
		}
		entrada.close();
	}
}
