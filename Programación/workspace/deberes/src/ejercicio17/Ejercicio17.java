package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entrada = new Scanner(System.in);
		System.out.println("¿Cuantas personas conforman su grupo de viaje?");
		int personas = entrada.nextInt();
		System.out.println("¿Cuantos kilometros van a recorrer?");
		int kilometros = entrada.nextInt();
		int descuento = 0;
		double precio = 20 * personas;
		if (kilometros > 200)
		{
			precio+=0.03*(kilometros-200);
		}
		if (kilometros >= 400)
		{
			descuento+=15;
		}
		if (personas >= 3)
		{
			descuento+=10;
		}
		precio = precio - ((double)precio*descuento/100); 
		System.out.printf("Serán %.2f€, gracias por elegirnos para su viaje.", precio);
	}

}
