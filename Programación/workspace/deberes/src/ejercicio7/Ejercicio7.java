package ejercicio7;

import java.util.Scanner;


public class Ejercicio7 
{
	public static void main (String[] args)
	{
		int entrada;
		int contador = 0;
		int sumpos = 1;
		int numpos = 0;
		int sumneg = 1;
		int numneg = 0;
		int numceros = 0;
		while (contador != 10)
		{
			System.out.println("Introduzca un número, quedan " + (10 - contador));
			Scanner inp = new Scanner(System.in);
			entrada = inp.nextInt();
			if (entrada > 0)
			{
				sumpos+=entrada;
				numpos++;
			}
			else if (entrada < 0)
			{
				sumneg+=entrada;
				numneg++;
			}
			else
			{
				numceros++;
			}
			contador++;
			inp.close();
		}
		System.out.println("El número de ceros es " + numceros + " la media de los positivos es " + (sumpos/numpos) + " la media de los negativos es " + (sumneg)/numneg);
	}
}
