package ejercicio3;

public class Ejercicio3 {
	public static void main(String[] args)
	{
		double z;
		int n = 10;
		int p = 4;
		int q = 2;
		System.out.println("int n = 10");
		System.out.println("int p = 4");
		System.out.println("int q = 2");
		System.out.println("z = n/p");
		z = n/p;
		System.out.println("z = " + z);
		System.out.println("Debido a que se trataria de una división entera");
		System.out.println("z = (double)n/p");
		z = (double)n/p;
		System.out.println("z = " + z);
		System.out.println("Debido a que se trata de una division racional");
		System.out.println("z = (double)(n/p)");
		z = (double)(n/p);
		System.out.println("z = " + z);
		System.out.println("Debido a que la division se realiza como entera antes de asociarse a racional");
		System.out.println("z+=n");
		z+=n;
		System.out.println("z = " + z);
		System.out.println("La operación =+ suma el valor anterior de z mas el valor de n y lo asigna a z");
		System.out.println("q*=z");
		q*=z;
		System.out.println("q = " + q);
		System.out.println("La operación *= multiplica el valor anterior de q y z y lo asigna a q");
		System.out.println("z+=2");
		z+=2;
		System.out.println("z = " + z);
		System.out.println("La operacion += asigna a z su anterior valor más 2");
	}
}