package ejercicio8;

import java.util.Scanner;

public class Ejercicio8 {

	public static void main(String[] args) 
	{
		int numero = 0;
		int contador = 0;
		Scanner entrada = new Scanner(System.in);
		while (numero >= 0 )
		{
			System.out.println("Introduzca un numero, menor que cero si lo que desea es salir.");
			numero = entrada.nextInt();
			if (numero < 0)
			{
				break;
			}
			if (numero % 10 == 2)
			{
				contador++;
			}
		}
		System.out.println("Se han leido un total de " + contador + " números terminados en cero.");
		entrada.close();
	}

}
