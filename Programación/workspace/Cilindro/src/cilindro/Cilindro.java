package cilindro;

import java.util.Scanner;

public class Cilindro 
{
	
	public static void main(String[] args) 
	{
		
	double altura;
	double radio;
	final double PI = 3.1415926535;
	System.out.println("Introduzca la altura");
	Scanner entrada = new Scanner(System.in);
	altura = entrada.nextDouble();
	System.out.println("Introduzca radio");
	radio = entrada.nextDouble();
	System.out.printf("El volumen es %.1000f %n", ((Math.pow(radio, 2) * PI) * altura));
	entrada.close();
	
	}
	
}

